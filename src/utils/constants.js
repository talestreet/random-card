const CARD_NUMBERS=['A','2','3','4','5','6','7','8','9','10','11','12','13'];
const COLORS=['SPADE','HEART','CLUB','DIAMOND']

export {
    CARD_NUMBERS,
    COLORS
}