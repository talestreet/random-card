const findRandomCard=({cards,colors})=>{
    const returnValue={
      colors,
      distributed:{}
    }
    const pickColor=Math.floor(Math.random() * colors.length);
    const pickCard=Math.floor(Math.random() * cards[pickColor].length);

    const selectedColorCards=[...cards[pickColor]];

    returnValue.distributed={
      color:colors[pickColor].toLowerCase(),
      number:selectedColorCards.splice(pickCard,1)[0]
    }
    
    const newCards=[...cards]
    newCards[pickColor]=selectedColorCards
   
    if(selectedColorCards.length===0){
      const colorsTemp=[...colors]
      colorsTemp.splice(pickColor,1)
      newCards.splice(pickColor,1)
      returnValue.colors=colorsTemp
    }
    returnValue.cards=newCards
    return returnValue
  }
  export {
      findRandomCard
  }