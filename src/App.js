import './App.scss';
import { Button,Row,Container } from 'reactstrap';
import { useState } from 'react';
import DeckCard from './components/DeckCard';
import {CARD_NUMBERS,COLORS} from './utils/constants';
import {findRandomCard } from './utils/methods';
function createDataSet(){
  let arr=[];
  for(let i=0;i<CARD_NUMBERS.length;i++){
    let smallArr=[{
      number:CARD_NUMBERS[i],
      color:COLORS[0]
    },{
      number:CARD_NUMBERS[i],
      color:COLORS[1]
    },{
      number:CARD_NUMBERS[i],
      color:COLORS[2]
    },{
      number:CARD_NUMBERS[i],
      color:COLORS[3]
    }]
   
    arr=[...arr,...smallArr]
  }
  return arr;
}

function App() {
  const [cards,setCards]=useState(createDataSet())
  const [distributedCards,setDistributedCards]=useState([])
 
  const distributeCards=()=>{
    
    let tempDistributed=[...distributedCards]
    const tempCards=[...cards];
    const max=tempCards.length <5 ? tempCards.length : 5
    for(let i=0;i<max;i++){
      const randomIndex=Math.floor(Math.random() * tempCards.length);
      tempDistributed=[...tempDistributed,tempCards.splice(randomIndex,1)[0]]
    }
    setCards(tempCards)
    setDistributedCards(tempDistributed)
  }
  function renderDistributed(){
    if(!distributedCards || distributedCards.length<=0) return null;
    return distributedCards.map((card)=>{
      return (<DeckCard key={card.color + card.number} {...card}></DeckCard>)
    })
  }
  return (
    <Container>
        <Button color="primary" className=" mt-4 mb-4" onClick={distributeCards} disabled={distributedCards.length>=52}>Distribute</Button>
        <Row>{renderDistributed()}</Row>
    </Container>
  );
}

export default App;
