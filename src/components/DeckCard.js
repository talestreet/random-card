import {Card,Col} from 'reactstrap';
import club from '../assets/club.png';
import diamond from '../assets/diamond.png';
import heart from '../assets/heart.png';
import spade from '../assets/spade.png';
export default function({color,number}){
    return (
        <Col lg="3" sm="4" className="deckCard__column mb-4"> 
            <Card>
                <div>{color=== 'SPADE' &&  <img className="deckCard__image" src={spade}/>}
                {color=== 'DIAMOND' &&  <img className="deckCard__image" src={diamond}/>}
                {color=== 'HEART' &&  <img className="deckCard__image" src={heart}/>}
                {color=== 'CLUB' &&  <img className="deckCard__image" src={club}/>}
                <strong> {number}</strong>
                </div>
            </Card>
        </Col>
    )
}